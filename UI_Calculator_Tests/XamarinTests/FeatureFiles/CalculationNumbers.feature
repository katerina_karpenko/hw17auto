﻿Feature: CalculationNumbers
	As a user 
	I want two calculate two number
	In order to get exact mathematical calculations

Background: 
	Given Calculator is opened


Scenario: Add two integer numbers
	When I click on the button one
	When I click on the button two
	When I click on the button plus
	When I click on the button three
	When I click on the button four
	When I click on the button Equal
	Then the result should be forty six

Scenario: Subtract two integer numbers
	When I click on the button eight
	When I click on the button five
	When I click on the button minus
	When I click on the button eight
	When I click on the button zero
	When I click on the button Equal
	Then the result should be five

Scenario: Multiply two integer numbers
	When I click on the button nine
	When I click on the button one
	When I click on the button multiplication 
	When I click on the button two
	When I click on the button Equal
	Then the result should be one hundred eighty two

Scenario: Divided two integer numbers
	When I click on the button seven
	When I click on the button two
	When I click on the button division
	When I click on the button eight
	When I click on the button Equal
	Then the result should be nine

Scenario: Add two not integer numbers
	When I click on the button one
	When I click on the button dot
	When I click on the button five
	When I click on the button plus
	When I click on the button two
	When I click on the button dot
	When I click on the button eight
	When I click on the button Equal
	Then the result should be four dot three

Scenario: Subtract two not integer numbers
	When I click on the button eight
	When I click on the button dot
	When I click on the button five
	When I click on the button minus
	When I click on the button six
	When I click on the button dot
	When I click on the button two
	When I click on the button Equal
	Then the result should be two dot three

Scenario: Multiply two not integer numbers
	When I click on the button seven
	When I click on the button dot
	When I click on the button three
	When I click on the button multiplication 
	When I click on the button four
	When I click on the button dot
	When I click on the button nine
	When I click on the button Equal
	Then the result should be thirty five point seventy seven

Scenario: Divided two not integer numbers
	When I click on the button five
	When I click on the button dot
	When I click on the button three
	When I click on the button division
	When I click on the button two
	When I click on the button dot
	When I click on the button five
	When I click on the button Equal
	Then the result should be two dot twelve

Scenario: Deleting numbers
	When I click on the button one
	When I click on the button DEL
	Then Number deleted

Scenario: Divide on zero
	When I click on the button eight
	When I click on the button division
	When I click on the button zero
	When I click on the button Equal
	Then the result should be Infinity


Scenario: Set of calculations with parameters
	When I click on the first button <first> number
	When I click on the button on the math <operation> button
	When I click on the second button <second> number
	When I click on the button Equal
	Then I see the answer to the selected calculations <result>

	Examples: 
	| first | operation | second | result |
	| 1     | +         | 5      | 6      |
	| 5     | -         | 3      | 2      |
	| 8     | *         | 6      | 48     |
	| 9     | ÷         | 3      | 3      |