﻿using NUnit.Framework;
using System;
using TechTalk.SpecFlow;

namespace XamarinTests.Steps
{
    [Binding]
    public class CalculationNumbersSteps
    {
        MainScreen mainScreen = new MainScreen();


        [Given(@"Calculator is opened")]
        public void GivenCalculatorIsOpened()
        {
            AppInitializer.StartApp();
        }

        [When(@"I click on the button one")]
        public void WhenIClickOnTheButtonOne()
        {
            mainScreen.TapOnOne();
        }

        [When(@"I click on the button two")]
        public void WhenIClickOnTheButtonTwo()
        {
            mainScreen.TapOnTwo();
        }

        [When(@"I click on the button plus")]
        public void WhenIClickOnTheButtonPlus()
        {
            mainScreen.TapOnPlus();
        }

        [When(@"I click on the button three")]
        public void WhenIClickOnTheButtonThree()
        {
            mainScreen.TapOnThree();
        }

        [When(@"I click on the button four")]
        public void WhenIClickOnTheButtonFour()
        {
            mainScreen.TapOnFour();
        }

        [When(@"I click on the button Equal")]
        public void WhenIClickOnTheButtonEqual()
        {
            mainScreen.TapOnEqual();
        }

        [When(@"I click on the button eight")]
        public void WhenIClickOnTheButtonEight()
        {
            mainScreen.TapOnEight();
        }

        [When(@"I click on the button five")]
        public void WhenIClickOnTheButtonFive()
        {
            mainScreen.TapOnFive();
        }

        [When(@"I click on the button minus")]
        public void WhenIClickOnTheButtonMinus()
        {
            mainScreen.TapOnMinus();
        }

        [When(@"I click on the button zero")]
        public void WhenIClickOnTheButtonZero()
        {
            mainScreen.TapOnZero();
        }

        [When(@"I click on the button nine")]
        public void WhenIClickOnTheButtonNine()
        {
            mainScreen.TapOnNine();
        }

        [When(@"I click on the button multiplication")]
        public void WhenIClickOnTheButtonMultiplication()
        {
            mainScreen.TapOnMultiply();
        }

        [When(@"I click on the button seven")]
        public void WhenIClickOnTheButtonSeven()
        {
            mainScreen.TapOnSeven();
        }

        [When(@"I click on the button division")]
        public void WhenIClickOnTheButtonDivision()
        {
            mainScreen.TapOnDivide();
        }

        [When(@"I click on the button dot")]
        public void WhenIClickOnTheButtonDot()
        {
            mainScreen.TapOnComma();
        }

        [When(@"I click on the button six")]
        public void WhenIClickOnTheButtonSix()
        {
            mainScreen.TapOnSix();
        }

        [When(@"I click on the button DEL")]
        public void WhenIClickOnTheButtonDEL()
        {
            mainScreen.TapOnDEL();
        }

        [Then(@"the result should be forty six")]
        public void ThenTheResultShouldBeFortySix()
        {
            string result = mainScreen.GetTextFromField();
            Assert.AreEqual("46", result);
        }

        [Then(@"the result should be five")]
        public void ThenTheResultShouldBeFive()
        {
            string result = mainScreen.GetTextFromField();
            Assert.AreEqual("5", result);
        }

        [Then(@"the result should be one hundred eighty two")]
        public void ThenTheResultShouldBeOneHundredEightyTwo()
        {
            string result = mainScreen.GetTextFromField();
            Assert.AreEqual("182", result);
        }

        [Then(@"the result should be nine")]
        public void ThenTheResultShouldBeNine()
        {
            string result = mainScreen.GetTextFromField();
            Assert.AreEqual("9", result);
        }

        [Then(@"the result should be four dot three")]
        public void ThenTheResultShouldBeFourDotThree()
        {
            string result = mainScreen.GetTextFromField();
            Assert.AreEqual("4.3", result);
        }

        [Then(@"the result should be two dot three")]
        public void ThenTheResultShouldBeTwoDotThree()
        {
            string result = mainScreen.GetTextFromField();
            Assert.AreEqual("2.3", result);
        }

        [Then(@"the result should be thirty five point seventy seven")]
        public void ThenTheResultShouldBeThirtyFivePointSeventySeven()
        {
            string result = mainScreen.GetTextFromField();
            Assert.AreEqual("35.77", result);
        }

        [Then(@"the result should be two dot twelve")]
        public void ThenTheResultShouldBeTwoDotTwelve()
        {
            string result = mainScreen.GetTextFromField();
            Assert.AreEqual("2.12", result);
        }

        [Then(@"Number deleted")]
        public void ThenNumberDeleted()
        {
            string result = mainScreen.GetTextFromField();
            Assert.AreEqual("0", result);
        }

        [Then(@"the result should be Infinity")]
        public void ThenTheResultShouldBeInfinity()
        {
            string result = mainScreen.GetTextFromField();
            Assert.AreEqual("Infinity", result);
        }

        [When(@"I click on the first button (.*) number")]
        public void WhenIClickOnTheFirstButtonNumber(string p0)
        {
            mainScreen.TapOnButton(p0);
        }

        [When(@"I click on the button on the math (.*) button")]
        public void WhenIClickOnTheButtonOnTheMathButton(string p0)
        {
            mainScreen.TapOnButton(p0);
        }

        [When(@"I click on the second button (.*) number")]
        public void WhenIClickOnTheSecondButtonNumber(string p0)
        {
            mainScreen.TapOnButton(p0);
        }

        [Then(@"I see the answer to the selected calculations (.*)")]
        public void ThenISeeTheAnswerToTheSelectedCalculations(string p0)
        {
            string result = mainScreen.GetTextFromField();
            Assert.AreEqual(p0, result);
        }



    }
}
