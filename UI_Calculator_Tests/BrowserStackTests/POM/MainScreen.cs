﻿using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrowserStackTests.POM
{

    public class MainScreen
    {
        private AndroidDriver<AndroidElement> _driver;

        public MainScreen(AndroidDriver<AndroidElement> driver)
        {
            this._driver = driver;
        }

        public By textField = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.HorizontalScrollView/android.widget.TextView");
        public By btnDEL = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[1]");
        public By btnDivide = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[2]");

        public By btnSeven = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[3]");
        public By btnEight = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[4]");
        public By btnNine = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[5]");
        public By btnMultiply = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[6]");

        public By btnFour = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[7]");
        public By btnFive = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[8]");
        public By btnSix = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[9]");
        public By btnMinus = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[10]");

        public By btnOne = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[11]");
        public By btnTwo = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[12]");
        public By btnThree = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[13]");
        public By btnPlus = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[14]");

        public By btnComma = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[15]");
        public By btnZero = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[16]");
        public By btnEqual = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[17]");


        public MainScreen TapOnOne()
        {
            _driver.FindElement(btnOne).Click();
            return this;
        }

        public MainScreen TapOnTwo()
        {
            _driver.FindElement(btnTwo).Click();
            return this;
        }

        public MainScreen TapOnThree()
        {
            _driver.FindElement(btnThree).Click();
            return this;
        }

        public MainScreen TapOnFour()
        {
            _driver.FindElement(btnFour).Click();
            return this;
        }

        public MainScreen TapOnFive()
        {
            _driver.FindElement(btnFive).Click();
            return this;
        }

        public MainScreen TapOnSix()
        {
            _driver.FindElement(btnSix).Click();
            return this;
        }

        public MainScreen TapOnSeven()
        {
            _driver.FindElement(btnSeven).Click();
            return this;
        }

        public MainScreen TapOnEight()
        {
            _driver.FindElement(btnEight).Click();
            return this;
        }

        public MainScreen TapOnNine()
        {
            _driver.FindElement(btnNine).Click();
            return this;
        }

        public MainScreen TapOnZero()
        {
            _driver.FindElement(btnZero).Click();
            return this;
        }

        public MainScreen TapOnDEL()
        {
            _driver.FindElement(btnDEL).Click();
            return this;
        }

        public MainScreen TapOnPlus()
        {
            _driver.FindElement(btnPlus).Click();
            return this;
        }

        public MainScreen TapOnMinus()
        {
            _driver.FindElement(btnMinus).Click();
            return this;
        }

        public MainScreen TapOnMultiply()
        {
            _driver.FindElement(btnMultiply).Click();
            return this;
        }

        public MainScreen TapOnDivide()
        {
            _driver.FindElement(btnDivide).Click();
            return this;
        }

        public MainScreen TapOnComma()
        {
            _driver.FindElement(btnComma).Click();
            return this;
        }

        public MainScreen TapOnEqual()
        {
            _driver.FindElement(btnEqual).Click();
            return this;
        }

        public string GetTextFromField()
        {
            return _driver.FindElement(textField).Text;
        }

    }
}
