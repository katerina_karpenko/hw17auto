﻿using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrowserStackTests
{
    public static class BrowserStack
    {
        public static AppiumOptions caps;
        public static AndroidDriver<AndroidElement> driver;

        public static void StartInfoBrowserStack()
        {
            caps = new AppiumOptions();
            // Set your BrowserStack access credentials
            caps.AddAdditionalCapability("browserstack.user", "bsuser_er3dZq");
            caps.AddAdditionalCapability("browserstack.key", "GjZgmHYM1HNgBQHinu7x");

            // Set URL of the application under test
            caps.AddAdditionalCapability("app", "bs://8e55875bfacc4bb8841a8cfcbb21a323e4231c93");

            // Specify device and os_version
            caps.AddAdditionalCapability("device", "Google Pixel 2");
            caps.AddAdditionalCapability("os_version", "9.0");

            // Specify the platform name
            caps.PlatformName = "Android";

            // Set other BrowserStack capabilities
            caps.AddAdditionalCapability("project", "First CSharp project");
            caps.AddAdditionalCapability("build", "CSharp Android");
            caps.AddAdditionalCapability("name", "third_test");


            // Initialize the remote Webdriver using BrowserStack remote URL
            // and desired capabilities defined above
            driver = new AndroidDriver<AndroidElement>(new Uri("http://hub-cloud.browserstack.com/wd/hub"), caps);

        }
    }
}
