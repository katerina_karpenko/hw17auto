﻿using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppiumTests
{
    public static class AppInitializerAppium
    {
        public static AppiumOptions caps = new AppiumOptions();

        public static AndroidDriver<AndroidElement> driver;

        public static void StartInfoAppium()
        {
            caps.AddAdditionalCapability("app", @"C:\Users\prots\Desktop\Final\calculaor\bin\Release\com.companyname.calculaor-Signed.apk");
            caps.AddAdditionalCapability("deviceName", "Google Pixel 2");
            caps.AddAdditionalCapability("automationName", "UIAutomator2");
            caps.AddAdditionalCapability("platformVersion", "9.0");
            caps.AddAdditionalCapability("platformName", "Android");
            caps.AddAdditionalCapability("build", "CSharp Android_Sim");
            caps.AddAdditionalCapability("name", "Appium_test");

            driver = new AndroidDriver<AndroidElement>(new Uri("http://127.0.0.1:4723/wd/hub"), caps);
        }

    }
}
